// staletea
// Copyright (C) 2019 Jonas Franz
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package web

import (
	"encoding/gob"
	"gitea.com/jonasfranz/staletea/config"
	"gitea.com/jonasfranz/staletea/models"
	"github.com/gin-contrib/sessions"
	"github.com/gin-contrib/sessions/cookie"
	"github.com/gin-gonic/gin"
)

// StartServer will start the webserver at the given addresses
func StartServer(addr ...string) error {
	r := gin.Default()
	gob.Register(new(models.User))
	store := cookie.NewStore([]byte(config.SessionSecret.Get().(string)))
	r.Use(sessions.Sessions("sessions", store))
	r.LoadHTMLFiles("templates/dashboard.tmpl")
	setupRoutes(r)
	return r.Run(addr...)
}

func setupRoutes(r *gin.Engine) {
	r.GET("/", showDashboard)
	r.POST("/", handleActivate)
	r.GET("/login", handleLogin)
	r.GET("/callback", handleCallback)
}
