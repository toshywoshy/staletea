// staletea
// Copyright (C) 2019 Jonas Franz
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package config

import (
	"gitea.com/jonasfranz/staletea/utils"
	"github.com/spf13/viper"
	"os"
)

// Key represents a config key
type Key string

// Get the value of the key from config
func (k Key) Get() interface{} {
	return viper.Get(string(k))
}

// Set the value of the key to config
func (k Key) Set(value interface{}) {
	viper.Set(string(k), value)
}

func (k Key) setDefault(defaultValue interface{}) {
	viper.SetDefault(string(k), defaultValue)
}

// This block contains all config keys
const (
	DaysUntilStale Key = "daysUntilStale"
	DaysUntilClose Key = "daysUntilClose"
	LabelBlacklist Key = "onlyLabels"
	LabelWhitelist Key = "exemptLabels"
	StaleLabel     Key = "staleLabel"

	MarkComment   Key = "markComment"
	UnmarkComment Key = "unmarkComment"
	CloseComment  Key = "closeComment"

	TypeBlacklist Key = "only"

	BaseURL Key = "host"

	GiteaURL          Key = "gitea.url"
	GiteaClientID     Key = "gitea.client_id"
	GiteaClientSecret Key = "gitea.client_secret"

	SessionSecret Key = "session_secret"
	DatabasePath  Key = "./database_path"
)

// SetupConfig fills all default values, reads the config and/or writes the default config.
func SetupConfig() error {
	DaysUntilStale.setDefault(60)
	DaysUntilClose.setDefault(7)
	LabelBlacklist.setDefault([]string{})
	LabelWhitelist.setDefault([]string{})
	StaleLabel.setDefault("wontfix")

	MarkComment.setDefault(`This issue has been automatically marked as stale because it has not had
  recent activity. It will be closed in %d days if no further activity occurs. Thank you
  for your contributions.`)
	UnmarkComment.setDefault("")
	CloseComment.setDefault("This issue was closed automatically since it was marked as stale and had no recent activity.")
	TypeBlacklist.setDefault([]string{})
	BaseURL.setDefault("http://localhost")
	GiteaURL.setDefault("https://gitea.com")
	GiteaClientID.setDefault("")
	GiteaClientSecret.setDefault("")

	secret, err := utils.NewSecret(32)
	if err != nil {
		return err
	}
	SessionSecret.setDefault(secret)
	DatabasePath.setDefault("staletea.db")

	viper.SetConfigName("config")
	viper.SetConfigType("yml")
	viper.AddConfigPath(".")
	if err := viper.SafeWriteConfigAs("config.yml"); err != nil {
		if os.IsNotExist(err) {
			if err := viper.WriteConfigAs("config.yml"); err != nil {
				return err
			}
		}
	}
	return viper.ReadInConfig()
}
